package cli;

import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

public interface IExecutable {
    void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException;
}
