package cli.command;

import manager.DraftManager;
import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import players.Player;
import players.Position;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

import java.util.List;
import java.util.stream.Collectors;

public class TopCmd implements cli.IExecutable {

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        List<Player> topPlayers = new DraftManager(playerData, state, history, info)
                .getTopPlayers((int)info.getPositions().getAllPositions().keySet().stream()
                        .filter(x -> x != Position.BENCH && x != Position.RB_WR_TE)
                        .count());

        System.out.println(String.format("%s", String.join("\n",
                        topPlayers.stream()
                                .map(x -> String.format("\t%s : %s - %f", x.getName(), x.getPosition(), x.getPointDistribution().getAverage()))
                                .collect(Collectors.toList())
                )));
    }
}
