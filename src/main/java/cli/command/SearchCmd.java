package cli.command;

import cli.IExecutable;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import manager.DraftManager;
import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import players.Player;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

import java.util.List;

@Parameters()
public class SearchCmd implements IExecutable {
    @Parameter(description = "player name")
    private List<String> name;

    public List<String> getName() {
        return name;
    }

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        List<Player> players = new DraftManager(playerData, state, history, info).findPlayers(name);
        for(Player player : players) {
            System.out.format("%s : %s - %f\n", player.getName(), player.getPosition(), player.getPointDistribution().getAverage());
        }
    }
}