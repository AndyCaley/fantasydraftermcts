package cli.command;

import cli.IExecutable;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import manager.DraftManager;
import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

import java.util.List;

@Parameters()
public class InitCmd implements IExecutable {
    @Parameter(description = "list of teamnames")
    private List<String> teams;

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        new DraftManager(playerData, state, history, info).initTeams(teams);
    }
}

