package cli.command;

import com.beust.jcommander.Parameter;
import manager.DraftManagerException;
import manager.LeagueInfo;
import manager.PredictionExploreExploitManager;
import manager.PredictionMctsManager;
import players.IPlayerData;
import players.Player;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

public class BestCmd implements cli.IExecutable {
    @Parameter(description = "debugFile", names = {"-d"})
    private String debugFile = null;

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {

        OutputStream debugStream = null;
        if(debugFile != null) {
            try {
                debugStream = new FileOutputStream(debugFile);
            } catch (FileNotFoundException e) {
                System.err.format("Could not open debug outputfile: %s", debugFile);
            }
        }

        List<Player> bestPlayers = new PredictionExploreExploitManager(state, info, simulator).getBestPlayers(3, debugStream);

        System.out.println(String.format("%s", String.join("\n",
                bestPlayers.stream()
                        .map(x -> String.format("\t%s : %s - %f", x.getName(), x.getPosition(), x.getPointDistribution().getAverage()))
                        .collect(Collectors.toList())
        )));
    }
}
