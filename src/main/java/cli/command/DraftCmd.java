package cli.command;

import cli.IExecutable;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import manager.DraftManager;
import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import players.Player;
import simulation.ISeasonSimulator;
import state.Draft;
import state.DraftHistory;
import state.DraftState;

import java.util.List;

@Parameters()
public class DraftCmd implements IExecutable {
    @Parameter(description = "player name")
    private List<String> name;

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        Draft draft = new DraftManager(playerData, state, history, info).draftPlayer(name);
        Player player = playerData.getPlayerData().stream().filter(x -> x.getId() == draft.getPlayerId()).findFirst().get();
        System.out.format("%s : %s - %f -> %s\n", player.getName(), player.getPosition(), player.getPointDistribution().getAverage(), draft.getTeam());
    }
}
