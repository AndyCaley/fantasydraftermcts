package cli.command;

import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

public class UndoCmd implements cli.IExecutable {
    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        if(history.getDrafts().size() == 0){
            throw new DraftManagerException(String.format("No drafts to undo"));
        }

        history.getDrafts().remove(history.getDrafts().size() - 1);
    }
}
