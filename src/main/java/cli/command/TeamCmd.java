package cli.command;

import com.beust.jcommander.Parameter;
import league.FantasyTeam;
import manager.DraftManager;
import manager.DraftManagerException;
import manager.LeagueInfo;
import players.IPlayerData;
import simulation.ISeasonSimulator;
import state.DraftHistory;
import state.DraftState;

import java.util.Map;
import java.util.stream.Collectors;

public class TeamCmd implements cli.IExecutable {
    @Parameter
    private String team;

    @Override
    public void run(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info, ISeasonSimulator simulator) throws DraftManagerException {
        FantasyTeam team = new DraftManager(playerData, state, history, info).getTeam(this.team);
        Map<FantasyTeam, Float> teamResults = simulator.simulateWins(100, info.getSchedule().getSchedule(info.getNumWeeks(), state.getFantasyTeams()));

        System.out.println(String.format("%s: wins: %f\n%s",
                team.getName(),
                teamResults.get(team),
                String.join("\n",
                team.getPlayers().stream()
                        .map(x -> String.format("\t%s : %s - %f", x.getName(), x.getPosition(), x.getPointDistribution().getAverage()))
                        .collect(Collectors.toList())
                )));
    }
}
