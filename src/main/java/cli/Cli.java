package cli;

import cli.command.*;
import com.beust.jcommander.JCommander;
import com.google.gson.Gson;
import league.*;
import manager.DraftManagerException;
import manager.LeagueInfo;
import org.apache.commons.io.FileUtils;
import players.CsvPlayerData;
import players.IPlayerData;
import players.Player;
import simulation.*;
import state.Draft;
import state.DraftHistory;
import state.DraftState;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Cli {

    public static final String PLAYER_DATA_FILE = "2017_draftday.csv";
    public static final String DRAFT_DATA_FILE = ".draft.json";

    public static void main(String[] args) throws IOException {

        JCommander.Builder builder = JCommander.newBuilder();
        Map<String, IExecutable> commands = buildCommands();
        for(Map.Entry<String, IExecutable> cmd : commands.entrySet()) {
            builder.addCommand(cmd.getKey(), cmd.getValue());
        }

        JCommander jc = builder.build();
        jc.parse(args);

        IPlayerData playerData = new CsvPlayerData(Cli.class.getResourceAsStream(PLAYER_DATA_FILE));
        DraftState state = null;
        DraftHistory history = null;
        ISeasonSimulator simulator = null;
        LeagueInfo info = new LeagueInfo();

        File dataFile = new File(DRAFT_DATA_FILE);
        if(dataFile.exists()) {
            history = new Gson().fromJson(FileUtils.readFileToString(dataFile), DraftHistory.class);
            state = new DraftState(1, playerData.getPlayerData(), createTeams(history.getTeamNames()));
            replayHistory(playerData, history, state);
            info.setOrder(new SnakeDraftOrder());
            info.setPositions(new ExpediaLeaguePositions());
            info.setSchedule(new YahooSchedule());
            info.setNumWeeks(13);
            simulator = new SeasonSimulator(new BestLineupChooser(info.getPositions().getAllPositions()), new TriangleDistributionSampler());

        } else {
            history = new DraftHistory();
        }

        try {
            commands.get(jc.getParsedCommand()).run(playerData, state, history, info, simulator);
            FileUtils.writeStringToFile(dataFile, new Gson().toJson(history));

        } catch (DraftManagerException dme) {
            System.err.println(dme.getMessage());
        }
    }

    private static void replayHistory(IPlayerData playerData, DraftHistory history, DraftState state) {
        List<Player> data = playerData.getPlayerData();
        List<FantasyTeam> teams = state.getFantasyTeams();
        for(Draft draft : history.getDrafts()) {
            state.draftPlayerToTeam(data.stream().filter(x -> x.getId() == draft.getPlayerId()).findFirst().get(),
                    teams.stream().filter(x -> x.getName().equals(draft.getTeam())).findFirst().get());
        }
    }

    private static List<FantasyTeam> createTeams(List<String> teamNames) {
        List<FantasyTeam> teams = new ArrayList<>();

        for(String teamName : teamNames) {
            teams.add(new FantasyTeam(teamName));
        }
        return teams;
    }

    private static Map<String, IExecutable> buildCommands() {
        Map<String, IExecutable> cmds = new HashMap<>();

        cmds.put("init", new InitCmd());
        cmds.put("search", new SearchCmd());
        cmds.put("draft", new DraftCmd());
        cmds.put("undo", new UndoCmd());
        cmds.put("team", new TeamCmd());
        cmds.put("top", new TopCmd());
        cmds.put("best", new BestCmd());

        return cmds;
    }
}
