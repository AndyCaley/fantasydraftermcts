package players;

public enum Position {
    RB,
    WR,
    QB,
    TE,
    K,
    DST,

    DB,
    DL,
    LB,

    BENCH,
    RB_WR_TE
}
