package players;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvPlayerData implements IPlayerData {

    private static final String [] FILE_HEADER_MAPPING = {
            "playerId",
            "player",
            "team",
            "playerposition",
            "age",
            "exp",
            "bye",
            "points",
            "lower",
            "upper",
            "sdPts",
            "positionRank",
            "dropoff",
            "tier",
            "ptSpread",
            "overallECR",
            "positionECR",
            "sdRank",
            "risk",
            "games",
            "ptsGame",
            "vor",
            "overallRank",
            "vorLow",
            "vorHigh",
            "vorGame",
            "vorGameLow",
            "vorGameHigh",
            "adp",
            "auctionValue",
            "adpDiff",
            "cost",
            "sleeper"};

    private List<Player> playerData = new ArrayList<>();

    public CsvPlayerData(InputStream input) throws IOException {
        CSVParser parser = new CSVParser(new InputStreamReader(input), CSVFormat.DEFAULT
                .withSkipHeaderRecord()
                .withIgnoreEmptyLines()
                .withHeader(FILE_HEADER_MAPPING));

        for(CSVRecord record : parser.getRecords()) {
            Player player = new Player();
            player.setId(Long.parseLong(record.get("playerId")));
            player.setName(record.get("player"));
            player.setTeam(record.get("team"));
            player.setPosition(Position.valueOf(record.get("playerposition")));
            player.setPointDistribution(new Distribution(
                    Float.parseFloat(record.get("points")),
                    Float.parseFloat(record.get("lower")),
                    Float.parseFloat(record.get("upper"))));
            if (!record.get("bye").equals("NA")) {
                player.setBye(Integer.parseInt(record.get("bye")));
            } else {
                player.setBye(-1);
            }
            player.setDraftRank(Integer.parseInt(record.get("positionRank")));

            playerData.add(player);
        }
    }

    @Override
    public List<Player> getPlayerData() {
        return playerData;
    }
}
