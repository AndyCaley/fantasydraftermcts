package players;

import players.Player;

import java.util.List;
import java.util.Map;

public interface IPlayerData {
    List<Player> getPlayerData();
}
