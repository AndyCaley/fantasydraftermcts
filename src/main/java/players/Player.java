package players;

public class Player {
    private long id;
    private String name;
    private String team;
    private Position position;
    private Distribution pointDistribution;
    private int bye;
    private int draftRank;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getTeam() {
        return team;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public void setPointDistribution(Distribution pointDistribution) {
        this.pointDistribution = pointDistribution;
    }

    public Distribution getPointDistribution() {
        return pointDistribution;
    }

    public void setBye(int bye) {
        this.bye = bye;
    }

    public int getBye() {
        return bye;
    }

    public void setDraftRank(int draftRank) {
        this.draftRank = draftRank;
    }

    public int getDraftRank() {
        return draftRank;
    }
}
