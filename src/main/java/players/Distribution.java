package players;

public class Distribution {
    private float average;
    private float lower;
    private float upper;

    public Distribution(float average, float lower, float upper) {
        this.average = average;
        this.lower = lower;
        this.upper = upper;

        if(!(upper >= average && average >= lower)){
            float deviation = getDeviation();
            this.upper = average + deviation/2f;
            this.lower = average - (deviation/2f);
        }
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public float getLower() {
        return lower;
    }

    public void setLower(float lower) {
        this.lower = lower;
    }

    public float getUpper() {
        return upper;
    }

    public void setUpper(float upper) {
        this.upper = upper;
    }

    public float getDeviation() {
        return upper - lower;
    }
}
