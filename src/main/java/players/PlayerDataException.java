package players;

public class PlayerDataException extends Throwable {
    public PlayerDataException(String format) {
        super(format);
    }
}
