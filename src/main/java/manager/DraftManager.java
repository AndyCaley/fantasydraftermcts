package manager;

import league.FantasyTeam;
import league.IDraftOrder;
import players.IPlayerData;
import players.Player;
import state.Draft;
import state.DraftHistory;
import state.DraftState;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class DraftManager {

    private IPlayerData playerData;
    private DraftState state;
    private DraftHistory history;
    private LeagueInfo info;

    public DraftManager(IPlayerData playerData, DraftState state, DraftHistory history, LeagueInfo info) {
        this.playerData = playerData;
        this.state = state;
        this.history = history;
        this.info = info;
    }

    public void initTeams(List<String> teams) throws DraftManagerException {
        if(!history.getTeamNames().isEmpty()){
            throw new DraftManagerException("teams already exist");
        }
        history.setTeamNames(teams);
    }

    public List<Player> findPlayers(List<String> searchParameters) {
        return findPlayers(searchParameters, playerData.getPlayerData().stream());
    }

    private List<Player> findPlayers(List<String> searchParameters, Stream<Player> playerData) {
        String regex = "";
        for(int i = 0; i < searchParameters.size(); i++){
            regex += ".*" + searchParameters.get(i).toLowerCase();
        }
        final String searchString = regex + ".*";

        return playerData.filter(player ->
                player.getName().toLowerCase().matches(searchString)
        ).collect(Collectors.toList());
    }

    public Draft draftPlayer(List<String> searchParameters) throws DraftManagerException {
        List<Player> players = findPlayers(searchParameters, state.getRemainingPlayers().stream());


        if(players.size() > 1) {
            String firstName = players.get(0).getName();
            if (players.stream().allMatch(x -> x.getName().equals(firstName))) {
                players = players.stream()
                        .sorted((p1, p2) -> Float.compare(p2.getPointDistribution().getAverage(), p1.getPointDistribution().getAverage()))
                        .limit(1)
                        .collect(Collectors.toList());
            }
        }

        if(players.size() == 1) {
            FantasyTeam team = info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
            state.draftPlayerToTeam(players.get(0), team);
            Draft draft = new Draft();
            draft.setPlayerId(players.get(0).getId());
            draft.setTeam(team.getName());
            history.getDrafts().add(draft);
            return draft;
        }

        throw new DraftManagerException(
                String.format("Could not draft, players matched:\n%s",
                        String.join("\n", players.stream()
                            .map(x -> String.format("%s : %s - %f", x.getName(), x.getPosition(), x.getPointDistribution().getAverage()))
                            .collect(Collectors.toList())))
        );
    }

    public FantasyTeam getTeam(String teamName) {
        Optional<FantasyTeam> team = state.getFantasyTeams().stream().filter(x -> x.getName().equals(teamName)).findFirst();
        if(team.isPresent()){
            return team.get();
        }

        return info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
    }

    public List<Player> getTopPlayers(int num) {
        return state.getRemainingPlayers().stream()
                .limit(num)
                .collect(Collectors.toList());
    }
}
