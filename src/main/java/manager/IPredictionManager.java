package manager;

import players.Player;

import java.io.OutputStream;
import java.util.List;

public interface IPredictionManager {
    List<Player> getBestPlayers(int numPlayers, OutputStream debugStream);
}
