package manager;


import league.FantasyTeam;
import players.Player;
import players.Position;
import prediction.MonteCarloTreeSearch;
import simulation.ISeasonSimulator;
import state.DraftState;

import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

public class PredictionMctsManager implements IPredictionManager {
    private final DraftState state;
    private final LeagueInfo info;
    private final ISeasonSimulator simulator;

    public PredictionMctsManager(DraftState state, LeagueInfo info, ISeasonSimulator simulator) {
        this.state = state;
        this.info = info;
        this.simulator = simulator;
    }

    public List<Player> getBestPlayers(int numPlayers, OutputStream debugStream) {

        MonteCarloTreeSearch search = new MonteCarloTreeSearch(
                5000,
                state -> getAllDecisions(state),
                (state, player) -> updateState(state, player),
                (state, player) -> simulate(state, player));

        return search.best(numPlayers, state, debugStream);
    }

    private Collection<Player> getAllDecisions(DraftState state) {

        return state.getRemainingPlayers().stream()
                .limit(25)
                .collect(Collectors.toList());
    }

    private void updateState(DraftState state, Player player) {
        FantasyTeam currentTeam = info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
        state.draftPlayerToTeam(player, currentTeam);
        FantasyTeam activeTeam = info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
        while(currentTeam != activeTeam) {
            draftNextStartingPlayer(activeTeam, state);
            activeTeam = info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
        }
    }

    private float simulate(DraftState state, Player pick) {
        DraftState simulationState = state.clone();
        updateState(simulationState, pick);
        finishDraft(simulationState);

        Map<FantasyTeam, Float> results = simulator.simulateWins(50,
                info.getSchedule().getSchedule(info.getNumWeeks(), simulationState.getFantasyTeams()));

        FantasyTeam activeTeam = info.getOrder().getActiveTeam(state.getTurn(), simulationState.getFantasyTeams());
        return results.get(activeTeam) / (float)info.getNumWeeks();
    }

    private void finishDraft(DraftState state) {
        //TODO: make this from the top 5 - 10
        Long teamSize = info.getPositions().getAllPositions().values().stream().mapToLong(x -> x).sum();

        for(int turn = state.getTurn(); turn <= teamSize * state.getFantasyTeams().size(); turn++) {

            FantasyTeam team = info.getOrder().getActiveTeam(state.getTurn(), state.getFantasyTeams());
            draftNextStartingPlayer(team, state);
        }
    }

    private void draftNextStartingPlayer(FantasyTeam team, DraftState state) {
        //TODO: this needs to clone the Long value so it can be modified locally
        Map<Position, Long> teamPositions = new HashMap<>(info.getPositions().getAllPositions());
        teamPositions.remove(Position.BENCH);
        teamPositions.remove(Position.RB_WR_TE);
        //TODO: this should decrement the count, then remove if 0
        for(Player player : team.getPlayers()) {
            if(teamPositions.containsKey(player.getPosition())) {
                teamPositions.remove(player.getPosition());
            }
        }

        Player player;
        if(teamPositions.size() == 0){
            player = state.getRemainingPlayers().stream()
                    .findFirst()
                    .get();
        } else {
            player = state.getRemainingPlayers().stream()
                    .filter(p -> teamPositions.containsKey(p.getPosition()))
                    .findFirst()
                    .get();
        }

        state.draftPlayerToTeam(player, team);
    }
}
