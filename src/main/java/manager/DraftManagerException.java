package manager;

public class DraftManagerException extends Exception {
    public DraftManagerException(String message) {
        super(message);
    }
}
