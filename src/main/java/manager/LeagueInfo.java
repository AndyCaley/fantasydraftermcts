package manager;

import league.IDraftOrder;
import league.ILeaguePositions;
import league.ISchedule;

public class LeagueInfo {
    private int numWeeks;
    private IDraftOrder order;
    private ILeaguePositions positions;
    private ISchedule schedule;

    public IDraftOrder getOrder() {
        return order;
    }

    public void setOrder(IDraftOrder order) {
        this.order = order;
    }

    public ILeaguePositions getPositions() {
        return positions;
    }

    public void setPositions(ILeaguePositions positions) {
        this.positions = positions;
    }

    public ISchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(ISchedule schedule) {
        this.schedule = schedule;
    }

    public int getNumWeeks() {
        return numWeeks;
    }

    public void setNumWeeks(int numWeeks) {
        this.numWeeks = numWeeks;
    }
}
