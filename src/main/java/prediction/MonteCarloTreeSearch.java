package prediction;

import players.Player;
import state.DraftState;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.stream.Collectors;

public class MonteCarloTreeSearch {

    private static final float C = 0.75f;

    public interface Decisions {
        Collection<Player> getAll(DraftState state);
    }
    public interface StateUpdater {
        void update(DraftState state, Player player);
    }

    public interface Simulator {
        float simulate(DraftState state, Player player);
    }

    private static class TreeNode {
        public TreeNode parent = null;
        public List<TreeNode> children = new ArrayList<>();

        public DraftState state;
        public Player pick;

        public int visits = 0;
        public float value = 0;

        public float getUpperConfidenceBound(float c) {
            float parentNodeVisits = 0;
            if(parent != null) {
                parentNodeVisits = parent.visits;
            }
            return (float)((value/visits) + c * Math.sqrt(Math.log(parentNodeVisits)/visits));
        }
    }

    private Random random = new Random();
    private Decisions decisions;
    private StateUpdater stateUpdater;
    private Simulator simulator;
    private int iterations;

    public MonteCarloTreeSearch(int iterations, Decisions decisions, StateUpdater updater, Simulator simulator) {
        this.decisions = decisions;
        this.stateUpdater = updater;
        this.simulator = simulator;
        this.iterations = iterations;
    }

    public List<Player> best(int topDecisions, DraftState state, OutputStream debugStream) {

        TreeNode root = new TreeNode();
        root.state = state;

        for(int i = 0; i < iterations; i++){
            TreeNode leaf = select(root);

            expand(leaf);

            //root node does not contain a pick
            if(leaf.pick != null) {
                simulate(leaf);
                backpropogate(leaf);
            }
        }

        if(debugStream != null) {
            OutputStreamWriter writer = new OutputStreamWriter(debugStream);
            try {
                outputTree(root, writer, 0);
                writer.close();
            } catch (IOException e) {
                System.err.format("debug output failed to write: " + e.getMessage());
            }
        }

        return root.children.stream()
                .sorted((n1,n2) -> Integer.compare(n2.visits, n1.visits))
                .limit(topDecisions)
                .map(x -> x.pick)
                .collect(Collectors.toList());
    }

    private void outputTree(TreeNode node, OutputStreamWriter debugStream, int depth) throws IOException {
        //write node
        StringBuilder indent = new StringBuilder("");
        for(int i = 0; i <depth; i++){
            indent.append("\t");
        }
        if(node.pick != null) {
            debugStream.write(String.format("%sPick: %s : %s - %f +- %f Visits: %d Value: %f\n",
                    indent,
                    node.pick.getName(),
                    node.pick.getPosition(),
                    node.pick.getPointDistribution().getAverage(),
                    node.pick.getPointDistribution().getDeviation(),
                    node.visits,
                    node.value
            ));
        }

        node.children.stream()
                .sorted((x1,x2) -> Integer.compare(x2.visits, x1.visits))
                .forEach(child -> {
                    try {
                        outputTree(child, debugStream, depth + 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private TreeNode select(TreeNode node) {
        if(node.children.isEmpty()) {
            return node;
        }

        Optional<TreeNode> unvisited = node.children.stream().filter(x -> x.visits == 0).findFirst();
        if(unvisited.isPresent()){
            return unvisited.get();
        }

        //return top UCB
        float maxUcb = Float.MIN_VALUE;
        TreeNode child = null;
        for(int i = 0; i < node.children.size(); i++) {
            float ucb = node.children.get(i).getUpperConfidenceBound(C);
            if(ucb > maxUcb) {
                child = node.children.get(i);
                maxUcb = ucb;
            }
        }

        //pick based on ucb probability
/*
        TreeNode child = node.children.get(node.children.size() - 1);
        float sum = (float) node.children.stream().mapToDouble(x -> x.getUpperConfidenceBound(C)).sum();
        float value = random.nextFloat() * sum;
        for(int i = 0; i < node.children.size(); i++){
            value -= node.children.get(i).getUpperConfidenceBound(C);
            if(value <= 0){
                child = node.children.get(i);
                break;
            }
        }
*/
        return select(child);
    }


    private void expand(TreeNode leaf) {
        if(leaf.parent != null && leaf.state == null) {
            leaf.state = leaf.parent.state.clone();
        }

        DraftState postState = leaf.state.clone();
        if(leaf.pick != null) {
            stateUpdater.update(postState, leaf.pick);
        }
        for(Player player : decisions.getAll(postState)) {
            TreeNode child = new TreeNode();
            leaf.children.add(child);
            child.parent = leaf;
            child.pick = player;
            child.state = postState;
        }
    }

    private void simulate(TreeNode leaf) {
        leaf.visits++;
        leaf.value = simulator.simulate(leaf.state, leaf.pick);
    }

    private void backpropogate(TreeNode leaf) {
        TreeNode node = leaf.parent;
        float value = leaf.value;
        while(node != null){
            node.visits++;
            node.value += value;
            node = node.parent;
        }
    }
}
