package prediction;

import players.Player;
import state.DraftState;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UcbSearch {

    private final static float C = 1.0f;

    public interface Decisions {
        Collection<Player> getAll(DraftState state);
    }

    public interface StateUpdater {
        void update(DraftState state, Player player);
    }

    public interface Simulator {
        float simulate(DraftState state);
    }

    private static class Decision {
        public Player pick;

        public int visits = 0;
        public float value = 0;

        public float getUpperConfidenceBound(float totalPlays) {
            return (float)((value/visits) + C * Math.sqrt(Math.log(totalPlays)/visits));
        }
    }

    private Decisions decisions;
    private StateUpdater updater;
    private final Simulator simulator;
    private int iterations;


    public UcbSearch(int iterations, Decisions decisions, StateUpdater updater, Simulator simulator) {
        this.iterations = iterations;
        this.decisions = decisions;
        this.updater = updater;
        this.simulator = simulator;
    }

    public List<Player> best(int topDecisions, DraftState state, OutputStream debugStream) {

        Collection<Player> picks = decisions.getAll(state);

        List<Decision> decisions = picks.stream()
                .map(player -> {
                    Decision decision = new Decision();
                    decision.pick = player;
                    return decision;
                })
                .collect(Collectors.toList());

        for(int i = 0; i < iterations; i++) {
            Decision decision = select(decisions, i);
            DraftState simState = state.clone();
            updater.update(simState, decision.pick);
            simulate(decision, simState);
        }

        if(debugStream != null) {
            OutputStreamWriter writer = new OutputStreamWriter(debugStream);
            try {
                for(Decision decision : decisions.stream().sorted((i1,i2) -> Integer.compare(i2.visits, i1.visits)).collect(Collectors.toList())) {
                    writer.write(String.format("Pick: %s : %s - %f +- %f Visits: %d Value: %f Estimate: %f\n",
                            decision.pick.getName(),
                            decision.pick.getPosition(),
                            decision.pick.getPointDistribution().getAverage(),
                            decision.pick.getPointDistribution().getDeviation(),
                            decision.visits,
                            decision.value,
                            decision.value / decision.visits
                            ));
                }
                writer.close();
            } catch (IOException e) {
                System.err.format("debug output failed to write: " + e.getMessage());
            }
        }

        return decisions.stream()
                .sorted((i1,i2) -> Integer.compare(i2.visits, i1.visits))
                .limit(topDecisions)
                .map(x -> x.pick)
                .collect(Collectors.toList());
    }

    private Decision select(List<Decision> decisions, int totalPlays) {

        Optional<Decision> unvisited = decisions.stream()
                .filter(x -> x.visits == 0)
                .findAny();

        if(unvisited.isPresent()){
            return unvisited.get();
        }

        return decisions.stream()
                .sorted((i1, i2) -> Float.compare(i2.getUpperConfidenceBound(totalPlays), i1.getUpperConfidenceBound(totalPlays)))
                .findFirst()
                .get();
    }

    private void simulate(Decision decision, DraftState state) {
        decision.visits++;
        decision.value += simulator.simulate(state);
    }
}
