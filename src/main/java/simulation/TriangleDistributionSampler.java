package simulation;

import players.Distribution;

/**
 * Copyright 2017 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
public class TriangleDistributionSampler implements IDistributionSampler {
    @Override
    public double getValue(Distribution distribution) {
        float a = distribution.getLower();
        float b = distribution.getUpper();
        float c = distribution.getAverage();

        double F = (c - a) / (b - a);
        double rand = Math.random();
        if (rand < F) {
            return a + Math.sqrt(rand * (b - a) * (c - a));
        } else {
            return b - Math.sqrt((1 - rand) * (b - a) * (b - c));
        }    }
}
