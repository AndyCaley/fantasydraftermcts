package simulation;

import players.Distribution;

import java.util.Random;

/**
 * Copyright 2017 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
public class FlatDistributionSampler implements IDistributionSampler{

    private Random random = new Random();

    @Override
    public double getValue(Distribution distribution) {
        return distribution.getLower() + random.nextFloat() * (distribution.getDeviation());
    }
}
