package simulation;

import league.FantasyTeam;
import players.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SeasonSimulator implements ISeasonSimulator{

    private ILineupChooser lineupChooser;
    private IDistributionSampler sampler;

    public SeasonSimulator(ILineupChooser lineupChooser, IDistributionSampler sampler) {
        this.lineupChooser = lineupChooser;
        this.sampler = sampler;
    }

    public Map<FantasyTeam, Float> simulateWins(int iterations,
                                                Map<FantasyTeam, FantasyTeam[]> season) {

        Map<FantasyTeam, Float> wins = new HashMap<>();
        for(FantasyTeam team : season.keySet()) {
            wins.put(team, 0f);
        }

        for(Map.Entry<FantasyTeam,FantasyTeam[]> teamSeason : season.entrySet()) {
            FantasyTeam team = teamSeason.getKey();
            int week = 1;
            for(FantasyTeam opponent : teamSeason.getValue()) {
                Collection<Player> teamLineup = lineupChooser.getBestLineupForWeek(week, team);
                Collection<Player> opponentLineup = lineupChooser.getBestLineupForWeek(week, opponent);

                for(int iteration = 0; iteration < iterations; iteration++) {
                    float teamScore = computeBestScoreForWeek(week, teamLineup);
                    float opponentScore = computeBestScoreForWeek(week, opponentLineup);

                    if (teamScore > opponentScore) {
                        wins.put(team, wins.get(team) + (1.0f / iterations));
                    }
                }
                week++;
            }
        }

        return wins;
    }

    private float computeBestScoreForWeek(double week, Collection<Player> lineup) {

        float totalPoints = 0;
        for(Player player : lineup) {
            if(player.getBye() != week) {
                totalPoints += sampler.getValue(player.getPointDistribution());
            }
        }

        return totalPoints;
    }
}
