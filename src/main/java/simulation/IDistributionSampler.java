package simulation;

import players.Distribution;

/**
 * Copyright 2017 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
public interface IDistributionSampler {
    double getValue(Distribution distribution);
}
