package simulation;

import league.FantasyTeam;
import players.Player;
import players.Position;

import java.util.Collection;
import java.util.Map;

public interface ILineupChooser {
    Collection<Player> getBestLineupForWeek(int week, FantasyTeam team);
}
