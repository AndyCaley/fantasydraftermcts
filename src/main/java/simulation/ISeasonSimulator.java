package simulation;

import league.FantasyTeam;
import players.Position;

import java.util.Collection;
import java.util.Map;

public interface ISeasonSimulator {
    Map<FantasyTeam, Float> simulateWins(int iterations, Map<FantasyTeam, FantasyTeam[]> season);
}
