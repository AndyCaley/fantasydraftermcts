package simulation;

import players.Distribution;

import java.util.Random;

public class GaussianDistributionSampler implements IDistributionSampler {
    private Random random = new Random();

    public double getValue(Distribution distribution) {
        return (random.nextGaussian()*(distribution.getDeviation()*0.5f)) + distribution.getAverage();
    }
}
