package simulation;

import league.FantasyTeam;
import players.Player;
import players.Position;

import java.util.*;

public class BestLineupChooser implements ILineupChooser {

    private Map<Position, Long> positions;

    public BestLineupChooser(Map<Position, Long> positions) {
        this.positions = positions;
    }

    public Collection<Player> getBestLineupForWeek(int week, FantasyTeam team) {
        Set<Player> lineup = new HashSet<>();
        List<Player> sortedTeam = new ArrayList<>(team.getPlayers());
        sortedTeam.sort((x,y) ->
            Float.compare(y.getPointDistribution().getAverage(),
                    x.getPointDistribution().getAverage()));

        for(Map.Entry<Position,Long> entry : positions.entrySet()) {
            Position position = entry.getKey();
            Long needed = entry.getValue();

            if(position == Position.BENCH) {
                continue;
            }

            int found = 0;
            for(Player player : sortedTeam) {
                if(found >= needed) {
                    break;
                }

                if(player.getBye() == week) {
                    continue;
                }

                if(qualifiesForPosition(position, player) && !lineup.contains(player)) {
                    lineup.add(player);
                    found++;
                }
            }

        }

        return lineup;
    }

    private boolean qualifiesForPosition(Position position, Player player) {
        if(position == Position.RB_WR_TE) {
            return player.getPosition() == Position.RB
                    || player.getPosition() == Position.WR
                    || player.getPosition() == Position.TE;
        } else {
            return player.getPosition() == position;
        }
    }
}
