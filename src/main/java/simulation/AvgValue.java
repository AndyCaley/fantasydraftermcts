package simulation;

import players.Distribution;

/**
 * Copyright 2017 Expedia, Inc. All rights reserved.
 * EXPEDIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
public class AvgValue implements IDistributionSampler{
    @Override
    public double getValue(Distribution distribution) {
        return distribution.getAverage();
    }
}
