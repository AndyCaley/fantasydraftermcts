package state;

import java.util.ArrayList;
import java.util.List;

public class DraftHistory {
    private List<String> teamNames = new ArrayList<>();
    private List<Draft> drafts = new ArrayList<>();

    public List<String> getTeamNames() {
        return teamNames;
    }

    public void setTeamNames(List<String> teamNames) {
        this.teamNames = teamNames;
    }

    public List<Draft> getDrafts() {
        return drafts;
    }

    public void setDrafts(List<Draft> drafts) {
        this.drafts = drafts;
    }
}
