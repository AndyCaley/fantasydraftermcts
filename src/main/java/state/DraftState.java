package state;

import league.FantasyTeam;
import players.Player;

import java.util.*;

public class DraftState {
    private int turn;
    private List<Player> remainingPlayers;
    private List<FantasyTeam> fantasyTeams;

    public DraftState(int turn, List<Player> players, List<FantasyTeam> teams) {
        this.turn = turn;
        this.remainingPlayers = new ArrayList<>(players);
        this.fantasyTeams = teams;
    }

    public List<FantasyTeam> getFantasyTeams() {
        return fantasyTeams;
    }

    public List<Player> getRemainingPlayers() {
        return remainingPlayers;
    }

    public DraftState clone() {
        List<FantasyTeam> teams = new ArrayList<>();
        for(FantasyTeam team : fantasyTeams) {
            FantasyTeam newTeam = new FantasyTeam(team.getName());
            newTeam.setPlayers(new HashSet<>(team.getPlayers()));
            teams.add(newTeam);
        }

        return new DraftState(turn, new ArrayList<>(remainingPlayers), teams);
    }

    public int getTurn() {
        return turn;
    }

    public void draftPlayerToTeam(Player player, FantasyTeam fantasyTeam) {
        if(!remainingPlayers.remove(player)) {
            throw new RuntimeException(String.format("Tried to draft %s but couldn't", player));
        }
        fantasyTeam.getPlayers().add(player);
        turn++;
    }
}
