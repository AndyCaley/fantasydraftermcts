package league;

import players.Position;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExpediaLeaguePositions implements ILeaguePositions {
    private Map<Position, Long> positions = Stream.of(
            Position.QB,
            Position.RB,
            Position.RB,
            Position.WR,
            Position.WR,
            Position.TE,
            Position.RB_WR_TE,
            Position.K,
            Position.DST,

            Position.BENCH,
            Position.BENCH,
            Position.BENCH,
            Position.BENCH
    ).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    public Map<Position, Long> getAllPositions() {
        return positions;
    }

}
