package league;

import players.Position;

import java.util.Map;

public interface ILeaguePositions {
    Map<Position, Long> getAllPositions();
}
