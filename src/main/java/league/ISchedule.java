package league;

import javafx.util.Pair;

import java.util.List;
import java.util.Map;

public interface ISchedule {
    Map<FantasyTeam, FantasyTeam[]> getSchedule(int weeks, List<FantasyTeam> teams);
}
