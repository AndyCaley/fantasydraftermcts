package league;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YahooSchedule implements ISchedule {

    @Override
    public Map<FantasyTeam, FantasyTeam[]> getSchedule(int weeks, List<FantasyTeam> teams) {
        Map<FantasyTeam, FantasyTeam[]> schedule = new HashMap<>();
        for(FantasyTeam team : teams) {
            schedule.put(team, new FantasyTeam[weeks]);
        }

        for(int teamNum = 0; teamNum < teams.size() - 1; teamNum++) {
            FantasyTeam team = teams.get(teamNum);

            for(int week = 0; week < weeks; week++) {

                if (schedule.get(team)[week] != null) {
                    continue;
                }

                FantasyTeam opponent = teams.get((week + (teams.size() - 1 - teamNum) + 1) % (teams.size() - 1));

                if(opponent == team) {
                    opponent = teams.get(teams.size() - 1);
                }

                if(schedule.get(opponent)[week] != null) {
                    System.out.format("tried to double schedule: %s vs %s, week %d\n", team, opponent, week);
                    continue;
                }

                schedule.get(team)[week] = opponent;
                schedule.get(opponent)[week] = team;
            }
        }

        return schedule;
    }
}
