package league;

import java.util.List;

public class SnakeDraftOrder implements IDraftOrder {

    @Override
    public FantasyTeam getActiveTeam(int turn, List<FantasyTeam> teams) {
        int pos = (turn - 1) % (teams.size() * 2);
        if(pos >= teams.size()){
            pos = teams.size() - (pos - teams.size() + 1);
        }
        return teams.get(pos);
    }
}
