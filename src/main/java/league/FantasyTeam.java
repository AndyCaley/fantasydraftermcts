package league;

import players.Player;

import java.util.HashSet;
import java.util.Set;

public class FantasyTeam {
    private String name;
    private Set<Player> players = new HashSet<>();

    public FantasyTeam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "FantasyTeam: " + getName();
    }
}
