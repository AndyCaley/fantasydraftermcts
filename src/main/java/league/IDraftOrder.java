package league;

import java.util.List;

public interface IDraftOrder {
    FantasyTeam getActiveTeam(int turn, List<FantasyTeam> teams);
}
