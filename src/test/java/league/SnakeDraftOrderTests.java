package league;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SnakeDraftOrderTests {
    SnakeDraftOrder draftOrder;
    List<FantasyTeam> teams = Arrays.asList(
            new FantasyTeam("A"),
            new FantasyTeam("B"),
            new FantasyTeam("C"));

    @Before
    public void before(){
        draftOrder = new SnakeDraftOrder();
    }

    @Test
    public void firstGoesFirst(){
        FantasyTeam team = draftOrder.getActiveTeam(1, teams);
        assertEquals("A", team.getName());
    }

    @Test
    public void atEndRepeats(){
        FantasyTeam t1 = draftOrder.getActiveTeam(3, teams);
        FantasyTeam t2 = draftOrder.getActiveTeam(4, teams);

        assertEquals("C", t1.getName());
        assertEquals("C", t2.getName());
    }

    @Test
    public void goesBackwards(){
        FantasyTeam t1 = draftOrder.getActiveTeam(4, teams);
        FantasyTeam t2 = draftOrder.getActiveTeam(5, teams);

        assertEquals("C", t1.getName());
        assertEquals("B", t2.getName());
    }

    @Test
    public void atBeginningRepeats(){
        FantasyTeam t1 = draftOrder.getActiveTeam(7, teams);
        FantasyTeam t2 = draftOrder.getActiveTeam(6, teams);

        assertEquals("A", t1.getName());
        assertEquals("A", t2.getName());
    }

    @Test
    public void worksWith12Players() {
        List<FantasyTeam> teams12 = Arrays.asList(
                new FantasyTeam("1"),
                new FantasyTeam("2"),
                new FantasyTeam("3"),
                new FantasyTeam("4"),
                new FantasyTeam("5"),
                new FantasyTeam("6"),
                new FantasyTeam("7"),
                new FantasyTeam("8"),
                new FantasyTeam("9"),
                new FantasyTeam("10"),
                new FantasyTeam("11"),
                new FantasyTeam("12"));

        assertEquals("12", draftOrder.getActiveTeam(12, teams12).getName());
        assertEquals("12", draftOrder.getActiveTeam(13, teams12).getName());
    }
}
