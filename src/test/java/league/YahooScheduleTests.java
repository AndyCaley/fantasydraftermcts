package league;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;


public class YahooScheduleTests {

    @Test
    public void createsAccurateSchedule(){
        YahooSchedule yahooSchedule = new YahooSchedule();
        List<FantasyTeam> teams = Arrays.asList(
                new FantasyTeam("TeamNG"),
                new FantasyTeam("Bad Pass United"),
                new FantasyTeam("Check My Balls"),
                new FantasyTeam("Lauren's Legit Team"),
                new FantasyTeam("Fanciful Unicorns"),
                new FantasyTeam("Final Fantasy"),
                new FantasyTeam("King"),
                new FantasyTeam("Punjab Robotics"),
                new FantasyTeam("The Archangels"),
                new FantasyTeam("The Microbenchmarks"),
                new FantasyTeam("Zeno"),
                new FantasyTeam("David's Optimal Team")
        );
        Map<FantasyTeam, FantasyTeam[]> fullSchedule = yahooSchedule.getSchedule(13, teams);

        List<String> teamNgOppoenents = Arrays.stream(fullSchedule.get(teams.get(0))).map(x -> x.getName()).collect(Collectors.toList());
        assertEquals(Arrays.asList(
            "Bad Pass United",
            "Check My Balls",
            "Lauren's Legit Team",
            "Fanciful Unicorns",
            "Final Fantasy",
            "King",
            "Punjab Robotics",
            "The Archangels",
            "The Microbenchmarks",
            "Zeno",
            "David's Optimal Team",
            "Bad Pass United",
            "Check My Balls"
        ),
            teamNgOppoenents);

        List<String> badPassOpponents = Arrays.stream(fullSchedule.get(teams.get(1))).map(x -> x.getName()).collect(Collectors.toList());
        assertEquals(Arrays.asList(
                "TeamNG",
                "David's Optimal Team",
                "Check My Balls",
                "Lauren's Legit Team",
                "Fanciful Unicorns",
                "Final Fantasy",
                "King",
                "Punjab Robotics",
                "The Archangels",
                "The Microbenchmarks",
                "Zeno",
                "TeamNG",
                "David's Optimal Team"
                ),
                badPassOpponents);

        List<String> checkOpponents = Arrays.stream(fullSchedule.get(teams.get(2))).map(x -> x.getName()).collect(Collectors.toList());
        assertEquals(Arrays.asList(
                "Zeno",
                "TeamNG",
                "Bad Pass United",
                "David's Optimal Team",
                "Lauren's Legit Team",
                "Fanciful Unicorns",
                "Final Fantasy",
                "King",
                "Punjab Robotics",
                "The Archangels",
                "The Microbenchmarks",
                "Zeno",
                "TeamNG"
                ),
                checkOpponents);
    }
}
