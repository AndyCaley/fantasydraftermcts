import league.ExpediaLeaguePositions;
import league.FantasyTeam;
import league.SnakeDraftOrder;
import league.YahooSchedule;
import manager.IPredictionManager;
import manager.LeagueInfo;
import manager.PredictionMctsManager;
import org.junit.Test;
import players.CsvPlayerData;
import players.IPlayerData;
import players.Player;
import players.Position;
import simulation.BestLineupChooser;
import simulation.FlatDistributionSampler;
import simulation.SeasonSimulator;
import state.DraftState;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DraftTest {

    public static final String PLAYER_DATA_FILE = "test_rankings.csv";

    @Test
    public void test12PersonLeague() throws IOException {
        IPlayerData playerData = new CsvPlayerData(IPlayerData.class.getResourceAsStream(PLAYER_DATA_FILE));
        List<FantasyTeam> teams = Arrays.asList(
                new FantasyTeam("1"),
                new FantasyTeam("2"),
                new FantasyTeam("3"),
                new FantasyTeam("4"),
                new FantasyTeam("5"),
                new FantasyTeam("6"),
                new FantasyTeam("7"),
                new FantasyTeam("8"),
                new FantasyTeam("9"),
                new FantasyTeam("10"),
                new FantasyTeam("11"),
                new FantasyTeam("12")
        );

        DraftState state = new DraftState(1,
                playerData.getPlayerData(),
                teams
                );

        LeagueInfo info = new LeagueInfo();
        info.setNumWeeks(13);
        info.setSchedule(new YahooSchedule());
        info.setPositions(new ExpediaLeaguePositions());
        info.setOrder(new SnakeDraftOrder());

        SeasonSimulator sim = new SeasonSimulator(
                new BestLineupChooser(info.getPositions().getAllPositions()),
                new FlatDistributionSampler());

        IPredictionManager predictionManager = new PredictionMctsManager(state, info, sim);

        for(int team = 1; team <= info.getNumWeeks() * info.getPositions().getAllPositions().size(); team++){
            FantasyTeam activeTeam = info.getOrder().getActiveTeam(team, teams);

            if(activeTeam == teams.get(5)){
                List<Player> best = predictionManager.getBestPlayers(1, null);
                state.draftPlayerToTeam(best.get(0), activeTeam);
            } else {
                Map<Position, Long> teamPositions = new HashMap<>(info.getPositions().getAllPositions());
                teamPositions.remove(Position.BENCH);
                teamPositions.remove(Position.RB_WR_TE);
                for(Player player : activeTeam.getPlayers()) {
                    teamPositions.remove(player.getPosition());
                }

                Player player;
                if(teamPositions.size() == 0){
                    player = state.getRemainingPlayers().stream()
                            .findFirst()
                            .get();
                } else {
                    player = state.getRemainingPlayers().stream()
                            .filter(p -> teamPositions.containsKey(p.getPosition()))
                            .findFirst()
                            .get();
                }
                state.draftPlayerToTeam(player, activeTeam);
            }

        }

        Map<FantasyTeam, Float> results = sim.simulateWins(100,
                info.getSchedule().getSchedule(info.getNumWeeks(),
                state.getFantasyTeams()));

        for(Map.Entry<FantasyTeam, Float> entry : results.entrySet()) {
            System.out.format("%s: %f\n", entry.getKey(), entry.getValue());
        }
    }
}
