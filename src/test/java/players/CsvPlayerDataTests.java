package players;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;

public class CsvPlayerDataTests {
    @Test
    public void loadsPlayerData() throws PlayerDataException, IOException {
        InputStream testData = this.getClass().getResourceAsStream("test_rankings.csv");
        CsvPlayerData data = new CsvPlayerData(testData);
        assertNotNull(data.getPlayerData().get(1));
    }
}
