package simulation;

import javafx.geometry.Pos;
import league.FantasyTeam;
import org.junit.Test;
import players.Distribution;
import players.Player;
import players.Position;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class BestLineupChooserTests {
    @Test
    public void picksBestPlayerFromMultiple() {

        Map<Position, Long> positions = new HashMap<>();
        positions.put(Position.QB, 1L);

        BestLineupChooser chooser = new BestLineupChooser(positions);
        FantasyTeam team = new FantasyTeam("test");
        team.setPlayers(new HashSet<>());

        Player qb1 = new Player();
        qb1.setPosition(Position.QB);
        qb1.setBye(10);
        qb1.setPointDistribution(new Distribution(10, 9, 11));

        Player qb2 = new Player();
        qb2.setPosition(Position.QB);
        qb2.setBye(10);
        qb2.setPointDistribution(new Distribution(5, 4, 6));

        team.getPlayers().add(qb1);
        team.getPlayers().add(qb2);


        Collection<Player> bestLineupForWeek = chooser.getBestLineupForWeek(1, team);

        assertEquals(1, bestLineupForWeek.size());
        assertEquals(Arrays.asList(qb1), bestLineupForWeek);
    }

    @Test
    public void skipsByeWeekPlayers() {

        Map<Position, Long> positions = new HashMap<>();
        positions.put(Position.QB, 1L);
        BestLineupChooser chooser = new BestLineupChooser(positions);
        FantasyTeam team = new FantasyTeam("test");
        team.setPlayers(new HashSet<>());

        Player qb1 = new Player();
        qb1.setPosition(Position.QB);
        qb1.setBye(1);
        qb1.setPointDistribution(new Distribution(10, 9, 11));

        Player qb2 = new Player();
        qb2.setPosition(Position.QB);
        qb2.setBye(10);
        qb2.setPointDistribution(new Distribution(5, 4, 6));

        team.getPlayers().add(qb1);
        team.getPlayers().add(qb2);


        Collection<Player> bestLineupForWeek = chooser.getBestLineupForWeek(1, team);

        assertEquals(1, bestLineupForWeek.size());
        assertEquals(Arrays.asList(qb2), bestLineupForWeek);
    }
}
