package simulation;

import league.FantasyTeam;
import org.junit.Test;
import players.Distribution;
import players.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SeasonSimulatorTests {

    ILineupChooser mockLineupChooser = mock(ILineupChooser.class);

    @Test
    public void teamWithBetterPlayersWins() {
        SeasonSimulator sim = new SeasonSimulator(mockLineupChooser, new FlatDistributionSampler());

        Map<FantasyTeam, FantasyTeam[]> season = new HashMap<>();

        FantasyTeam team1 = new FantasyTeam("team1");
        FantasyTeam team2 = new FantasyTeam("team2");

        season.put(team1, new FantasyTeam[]{team2});
        season.put(team2, new FantasyTeam[]{team1});

        Player p1 = new Player();
        p1.setPointDistribution(new Distribution(8, 6, 10));

        Player p2 = new Player();
        p2.setPointDistribution(new Distribution(6, 4, 8));

        when(mockLineupChooser.getBestLineupForWeek(1, team1)).thenReturn(Arrays.asList(p1));
        when(mockLineupChooser.getBestLineupForWeek(1, team2)).thenReturn(Arrays.asList(p2));


        Map<FantasyTeam, Float> results = sim.simulateWins(1000, season);
        assertTrue(results.get(team1) > results.get(team2));
    }
}
